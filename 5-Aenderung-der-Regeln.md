# 5. Änderung der Regeln

    1. Alle Regeln können geändert werden. Ebenso können Regeln entfernt oder hinzugefügt werden.
    2. Bei der Änderung, einem Hinzufügen oder dem Entfernen einer Regel ist zu beachten, dass das neue Regelwerk kompatibel mit dem alten ist. Das heißt zum Beispiel, dass die Nummerierung der Regeln nicht geändert werden darf.
    3. Zur Änderung, dem Hinzufügen oder dem Entfernen einer Regel bedarf es einer Anfrage eines Nutzers in #regeln.
    4. Über die Änderung wird wie über die Verhängung einer Strafe abgestimmt. Allerdings ist das in 4.3.2 beschriebene Zeitfenster von 5 Minuten auf mindestens einen Tag, bei essentiellen Punkten auf mindestens eine Woche zu heben.
        1. Essentiell sind Veränderungen von Gesetzbuch 5 und Andere, die einen erheblichen Unterschied für die Kommunikation darstellen
    5. Eine Abstimmung zur Gesetzesänderung darf nicht zur Schulzeit durchgeführt werden. Schulzeit ist, wenn von mindestens einem Nutzer bekannt ist, dass er gerade Schule hat.
    6. Eine Abstimmung zur Gesetzesänderung darf nur im Zeitraum von 12:00 bis 22:30 deutscher Zeit durchgeführt werden. Ragt das vorgeschriebene Zeitfenster über 22:30 hinaus, muss die Entscheidung auf den nächsten Tag verschoben werden.
    7. Eine Änderung wird genau dann genehmigt, wenn eine zwei-drittel-Mehrheit der Regeländerung zustimmt