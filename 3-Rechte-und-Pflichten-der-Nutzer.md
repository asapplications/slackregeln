# 3. Rechte und Pflichten der Nutzer

    1. Jeder Nutzer darf in von ihm gewählten Channels sein. Er darf in keine Channels gezwungen werden.
        1. Ausnahme: Jeder muss in #announcements, #regeln und #general sein.
    2. Wenn ein Nutzer in Ruhe gelassen werden will (auch über DM), dann wird er in Ruhe gelassen.
    3. Statusnachrichten dürfen nicht angreifend oder verletzend sein.
    4. Profilbilder dürfen nicht angreifend oder verletzend sein.
