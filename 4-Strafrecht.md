# 4. Strafrecht

  1. Bestrafung ist nur bei einem Verstoß gegen mindestens eine der Regeln dieses Regelwerks möglich.
  2. Bestrafung durch

    1. Öffentliche Ansage des Verstoßes
    2. Temporärer Kick aus dem Channel (= Ausnahme für Regel 3.1)
    3. Temporäre Deaktivierung des Accounts
    4. In Extremfällen: Rechteentzug

  3. Über Strafen 4.2.3 und 4.2.4 muss abgestimmt werden.

    1. Hierbei schreibt der Kläger seine Klage inkl. der Forderung nach der Strafe in #regeln.
    2. Alle Nutzer haben nun mindestens 5 Minuten Zeit, mit (Strafe akzeptiert) oder (Strafe abgelehnt) als Reaction ihre Meinung zu zeigen. Der Angeklagte und der Kläger haben kein Stimmrecht.
    3. Die Strafe kann nur vollstreckt werden, wenn mehr Reactions als gemacht wurden.

  4. Sollte die Strafe nicht vollzogen werden können (z.B. weil der Angeklagte Primary Owner ist) wird über eine andere Strafe von gleicher Härte abgestimmt, die nicht unter 4.2 aufgelistet sein muss, aber kann.
