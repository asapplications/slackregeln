# 2. Verhaltensregeln in DMs und Channels

    1. Wir brauchen eigentlich keine Verhaltensregeln. Wir sind alle nett.
    2. Wir versuchen, dass Situationen möglichst nicht eskalieren.
    3. Erlaubt ist alles, was gesunder Menschenverstand als noch einigermaßen vernünftig einstufen kann.
        1. Ausnahme: kein Spam oder zu oft wiederholte Promotions
        2. In #spam und in der DM mit sich selbst oder @slackbot ist Spam erlaubt.
    4. In Channels wird nur über Dinge gechattet, die in keinen anderen Channel besser passen.
